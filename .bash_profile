if [[ "$OSTYPE" == "darwin"* ]]; then
	export BASH_SILENCE_DEPRECATION_WARNING=1
	eval "$(/opt/homebrew/bin/brew shellenv)"
	PATH="/opt/homebrew/opt/coreutils/libexec/gnubin:$PATH"
	alias tex-to-pdf="/Users/andrewhett/Programming/BASH/tex-to-pdf/tex-to-pdf"
	test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"
	export LDFLAGS="-L/opt/homebrew/lib"
	alias make=gmake
	export QUTE_CONFIG_DIR="~/.qutebrowser"
	alias gcc="gcc -I/opt/homebrew/include"
elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
	alias tex-to-pdf="/home/andrew/Programming/BASH/tex-to-pdf/tex-to-pdf"
fi

PS1="\$(
return_value=\$?;
printf \"╭╴\033[0;31m<\u@\h>\033[0m-\033[1;34m<\w\a>\033[0m-\033[0;33m<\$( ls $(2>/dev/null $PWD) | wc -l | sed 's/^ *//g' )>\033[0m\$( 2>/dev/null git branch | sed -n -e '/* /p' | sed -e 's/\(* \)//g' -e 's/^/\-\[\033[0;35m\]</' -e 's/$/>\[\033[0m\]/' )-\"
[ \$return_value = 0  ] && echo -e '\e[0;32m<\xE2\x9C\x93>' || echo -e '\e[0;31m<X>')\033[0m\n╰╴>"

eval "$(dircolors ~/.gruvbox.dircolors)"
PATH="$PATH:~/.cargo/bin"
alias ls=lsd

source ~/.opam/opam-init/init.sh

# GPG Agent
export GPG_TTY=$(tty)
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent
gpg-connect-agent updatestartuptty /bye > /dev/null
gpg-connect-agent "scd serialno" "learn --force" /bye > /dev/null
